﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {
	const int LAYER_GROUND = 9;
	const int LAYER_PLAYER = 10;
	const int LAYER_ENEMY = 11;

	public int crowdSize = 1000;
	public float maxSpeed = 10.0f;
	private float maxSpeedSquared;
	public Text timer;

	public float gameDuration = 60.0f;
	private float endTime;
	public Vector3 crowdSpawnExtents = new Vector3(40.0f, 0.0f, 40.0f);
	//--------------------------------------------------
	#region General Unity Lifecycle

	// Use this for initialization
	void Start () {
		maxSpeedSquared = maxSpeed * maxSpeed;
		GenerateCrowds();
		titlePanel.SetActive(true);
		titlePanel.GetComponent<Image>().CrossFadeAlpha(0.5f, 3.0f, true);
		StartCoroutine(ShowPlayerSelector());
	}
	IEnumerator ShowPlayerSelector()
	{
		yield return new WaitForSeconds(3.0f);
		playerSelector.SetActive(true);
	}
	public GameObject playerSelector;

	bool gameStarted = false;

	void StartGame()
	{
		gameStarted = true;

		InitPlayers();
		InitCamera();
		endTime = Time.time + gameDuration;
		scorePanel.SetActive(true);
		timer.gameObject.SetActive(true);
		instructions.SetActive(true);
		StartCoroutine(HideInstructions());
	}
	IEnumerator HideInstructions()
	{
		yield return new WaitForSeconds(12.0f);
		instructions.SetActive(false);
	}
	// Update is called once per frame
	void FixedUpdate () {
		UpdateCrowds();
		if (gameOver)
		{
			UpdateCamera();
		}
		else if (gameStarted)
		{
			UpdatePlayers();
		}
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Quit();
			return;
		}

		if (gameOver)
		{
		}
		else if (gameStarted)
		{
			UpdatePickups();
			UpdateTimer();
			UpdateCamera();
		}
		else if (!gameStarted)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				SetPlayersAndStart(1);
			}
			else if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				SetPlayersAndStart(2);
			}
			else if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				SetPlayersAndStart(3);
			}
			else if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				SetPlayersAndStart(4);
			}
		}
		UpdateCamera();
	}
	private int oldSecondsLeft = -1;

	void UpdateTimer()
	{
		float timeLeft = endTime - Time.time;
		int secondsLeft = Mathf.CeilToInt(timeLeft);
		if (secondsLeft < 0)
		{
			secondsLeft = 0;
		}

		if (secondsLeft != oldSecondsLeft)
		{
			timer.text = secondsLeft.ToString();
		}
		
		if (secondsLeft == 0)
		{
			GameOver();
		}
	}
	private bool gameOver = false;
	public GameObject gameOverScreen;
	public Text finalScore;
	public GameObject scorePanel;
	public GameObject titlePanel;
	public GameObject instructions;

	public void SetPlayersAndStart(int newPlayerCount)
	{
		titlePanel.SetActive(false);
		playerCount = newPlayerCount;
		StartGame();
	}
	public void Restart()
	{
		SceneManager.LoadSceneAsync(0);
	}

	public void MoreGames()
	{
		Application.OpenURL("http://idumpling.com");
		Application.Quit();
	}

	public void Quit()
	{
		Application.Quit();
	}

	void GameOver()
	{
		gameOver = true;
		gameOverScreen.SetActive(true);
		timer.gameObject.SetActive(false);
		for(int i = 0; i < playerKeyTexts.Length; i++)
		{
			if (playerKeyTexts[i] != null)
			{
				playerKeyTexts[i].gameObject.SetActive(false);
			}
		}
		if (playerCount == 1)
		{
			finalScore.text = "Final score: " + Mathf.RoundToInt(playerStats[0].z);
			scorePanel.SetActive(false);
		}
		else
		{
			float maxScore = -1.0f;
			int maxPlayerIndex = -1;
			for(int i = 0; i < playerCount; i++)
			{
				if (playerStats[i].z > maxScore)
				{
					maxScore = playerStats[i].z;
					maxPlayerIndex = i;
				}
			}
			finalScore.text = "Player " + (maxPlayerIndex + 1) + " wins!";
		}
	}

	#endregion

	//--------------------------------------------------
	#region Camera

	public Camera cam;
	private Transform camTran;
	public CinemachineTargetGroup cameraTargetGroup;

	public CinemachineVirtualCamera groupVCam;
	public CinemachineVirtualCamera mainVCam;

	void InitCamera()
	{
		CinemachineTargetGroup.Target[] targets = cameraTargetGroup.m_Targets;
		for(int i = 0; i < players.Length; i++)
		{
			CinemachineTargetGroup.Target target;
			target.target = players[i].transform;
			target.radius = 0.5f;
			target.weight = 1.0f;
			targets[i] = target;
		}
		cameraTargetGroup.m_Targets = targets;
	}

	void UpdateCamera()
	{
		bool useGroup = false;
		if (!gameOver && gameStarted)
		{
			CinemachineTargetGroup.Target[] m_targets = cameraTargetGroup.m_Targets;
			for (int i = 0; i < playerCount; i++)
			{
				CinemachineTargetGroup.Target camTarget = m_targets[i];
				camTarget.weight = Mathf.Lerp(camTarget.weight, players[i] == null ? 0.0f : 1.0f, Time.deltaTime * 0.5f);
				m_targets[i] = camTarget;
			}
			cameraTargetGroup.m_Targets = m_targets;
		}
		for (int i = 0; i < players.Length; i++)
		{
			if (players[i] != null)
			{
				useGroup = true;
				break;
			}
		}
		if (useGroup && !gameOver)
		{
			groupVCam.Priority = 11;
			mainVCam.Priority = 10;
		}
		else
		{
			groupVCam.Priority = 10;
			mainVCam.Priority = 11;
		}

	}
	#endregion

	//--------------------------------------------------
	#region Player

	const int PART_BEAK_BOTTOM = 0;
	const int PART_BEAK_TOP = 1;
	const int PART_LEG_L = 2;
	const int PART_LEG_R = 3;
	const int PART_TORSO = 4;
	const int PART_WING_L = 5;
	const int PART_WING_R = 6;
	const int PART_EYE_L = 7;
	const int PART_EYE_R = 8;
	const int PART_WINGFORCE_L = 9;
	const int PART_WINGFORCE_R = 10;

	public int playerCount;
	public Rigidbody[] players;
	public Vector3 leftWingTorque;
	public Vector3 rightWingTorque;
	public Rigidbody[] playerPrefabs;

	public KeyCode[] playerKeysL;
	public KeyCode[] playerKeysR;

	public string[] playerKeysDisplay;

	// x = life, y = popularity
	public Vector3[] playerStats; 

	public float wingForce = 100.0f;

	public Text keyTextPrefab;

	private Text[] playerKeyTexts;
	public Canvas keyTextCanvas;
	private Slider[] playerHealthSliders;

	public Text[] scoreTexts;
	public Text[] scoreSliders;

	void InitPlayers()
	{
		playerStats = new Vector3[playerCount];
		players = new Rigidbody[playerCount];
		playerKeyTexts = new Text[playerCount];
		playerHealthSliders = new Slider[playerCount];
		scoreSliders = new Text[playerCount];
		for (int i = 0; i < playerCount; i++)
		{
			float deg = i * 360 / playerCount;
			Vector3 spawnPoint = Quaternion.Euler(0.0f, deg, 0.0f) * Vector3.forward * playerCount + Vector3.up * 5.0f;
			players[i] = Instantiate<Rigidbody>(playerPrefabs[i], spawnPoint, Quaternion.Euler(0.0f, Random.value*360.0f, 0.0f));

			playerStats[i] = new Vector3(5.0f, crowdAttractionRange, 0.0f);

			playerKeyTexts[i] = Instantiate<Text>(keyTextPrefab, Vector3.zero, Quaternion.identity, keyTextCanvas.transform);
			playerKeyTexts[i].text = playerKeysDisplay[i];

			playerKeyTexts[i].transform.GetChild(0).GetComponent<Text>().text = "PLAYER " + (i + 1);
			playerHealthSliders[i] = playerKeyTexts[i].transform.GetChild(1).GetComponent<Slider>();
			playerHealthSliders[i].maxValue = 5.0f;
			playerHealthSliders[i].value = 5.0f;

			scoreTexts[i].gameObject.SetActive(true);
			scoreSliders[i] = scoreTexts[i].transform.GetChild(0).GetComponent<Text>();
		}
	}
	
	void UpdatePlayers()
	{
		for (int i = 0; i < players.Length; i++)
		{
			Rigidbody rb = players[i];
			if (rb == null) continue;

			Transform player = rb.transform;
			if (player == null || !player.gameObject.activeSelf)
				continue;

			Vector3 stats = playerStats[i];

			if (player.position.y < 0.75f)
			{
				stats.x -= Time.deltaTime;
				playerStats[i] = stats;
			}
			if (player.position.y < -5.0f)
			{
				stats.x = 0.0f;
			}
			playerHealthSliders[i].value = stats.x;

			// kill if down
			if (stats.x <= 0.0f)
			{
				player.position = Vector3.zero + Vector3.up * 5.0f;
				player.gameObject.SetActive(false);
				//CinemachineTargetGroup.Target[] m_targets = cameraTargetGroup.m_Targets;
				//CinemachineTargetGroup.Target camTarget = m_targets[i];
				//camTarget.weight = 0.0f;
				//m_targets[i] = camTarget;
				//cameraTargetGroup.m_Targets = m_targets;
				players[i] = null;
				playerKeyTexts[i].gameObject.SetActive(false);
				scoreTexts[i].color = Color.black;
				StartCoroutine(RespawnPlayer(i, rb));
				continue;
			}


			// left wing
			Vector3 rotTarget = new Vector3(-22f, 89f, -87f);
			if (Input.GetKey(playerKeysL[i]))
			{
				rotTarget = new Vector3(-13f, 177f, -90f);
			}

			Transform wingL = player.GetChild(PART_WING_L);
			wingL.localRotation = Quaternion.Slerp(wingL.localRotation, Quaternion.Euler(rotTarget), 10.0f*Time.deltaTime);
			if (Input.GetKeyDown(playerKeysL[i]))
			{
				Transform wingForceL = player.GetChild(PART_WINGFORCE_L);
				Vector3 wingForcePosition = wingForceL.position;
				Vector3 force = wingForceL.rotation * Vector3.forward * wingForce;
				force.y = 0.0f;
				rb.AddForceAtPosition(force, wingForcePosition, ForceMode.Impulse);

				rb.AddRelativeTorque(leftWingTorque, ForceMode.Impulse);
			}

			// right wing
			rotTarget = new Vector3(-22f, -89f, 87f);
			if (Input.GetKey(playerKeysR[i]))
			{
				rotTarget = new Vector3(-13f, -177f, 90f);
			}

			Transform wingR = player.GetChild(PART_WING_R);
			wingR.localRotation = Quaternion.Slerp(wingR.localRotation, Quaternion.Euler(rotTarget), 10.0f * Time.deltaTime);
			if (Input.GetKeyDown(playerKeysR[i]))
			{
				Transform wingForceR = player.GetChild(PART_WINGFORCE_R);
				Vector3 wingForcePosition = wingForceR.position;
				Vector3 force = wingForceR.rotation * Vector3.forward * wingForce;
				force.y = 0.0f;
				rb.AddForceAtPosition(force, wingForcePosition, ForceMode.Impulse);
				rb.AddRelativeTorque(rightWingTorque, ForceMode.Impulse);
			}

			Vector3 realForward = rb.rotation * Vector3.forward;
			Vector3 forward = realForward;
			forward.y = 0.0f;
			// lying on back
			Quaternion targetRotation = Quaternion.LookRotation(forward);
			rb.rotation = Quaternion.Slerp(rb.rotation, targetRotation, Time.deltaTime * 10.0f);

			// limit on speed
			float speedSqr = rb.velocity.sqrMagnitude;

			if (speedSqr > maxSpeedSquared)
			{
				rb.velocity = rb.velocity.normalized * maxSpeed;
			}

			playerKeyTexts[i].transform.position = cam.WorldToScreenPoint(rb.position + Vector3.up) + Vector3.up*5.0f;
		}
	}
	private IEnumerator RespawnPlayer(int playerIndex, Rigidbody player)
	{
		yield return new WaitForSeconds(5.0f);
		player.position = Vector3.zero + Vector3.up * 5.0f;
		player.gameObject.SetActive(true);

		//CinemachineTargetGroup.Target[] m_targets = cameraTargetGroup.m_Targets;
		//CinemachineTargetGroup.Target camTarget = m_targets[playerIndex];
		//camTarget.weight = 1.0f;
		//m_targets[playerIndex] = camTarget;
		//cameraTargetGroup.m_Targets = m_targets;
		players[playerIndex] = player;
		Vector3 stats = playerStats[playerIndex];
		stats.x = 5.0f;
		playerStats[playerIndex] = stats;
		playerHealthSliders[playerIndex].value = stats.x;
		playerKeyTexts[playerIndex].gameObject.SetActive(true);
		scoreTexts[playerIndex].color = Color.white;
	}
	#endregion

	//--------------------------------------------------
	#region Crowd

	public Rigidbody crowdPrefab;
	private Rigidbody[] crowds;
	private Vector4[] crowdsData;
	public Vector2 forceRange = new Vector2(5.0f, 10.0f);
	public Vector2 crowdRefreshTimeRange = new Vector2(1.0f, 6.0f);
	public float crowdTurnLerpRate = 3.0f;
	public Vector2 massRange = new Vector2(0.5f, 2.0f);
	public float crowdAttractionRange = 10.0f;

	void GenerateCrowds()
	{
		crowds = new Rigidbody[crowdSize];
		crowdsData = new Vector4[crowdSize];

		for (int i = 0; i < crowdSize; i++)
		{
			Vector3 pos = RandomCrowdPosition() +Vector3.up* Random.Range(0.0f, 3.0f);
			//pos.y = crowdPrefab.position.y + ;
			Quaternion rot = Quaternion.Euler(0.0f, Random.value * 360.0f, 0.0f);
			Rigidbody crowd = Instantiate(crowdPrefab, pos, rot) as Rigidbody;
			crowd.name = crowdPrefab.name;
			float girth = Random.Range(0.8f, 1.2f);
			float height = Random.Range(0.8f, 1.2f);

			crowd.transform.localScale = new Vector3(girth, height, girth);
			crowd.mass = RandomRange(massRange);
			crowds[i] = crowd;
			crowdsData[i] = Vector4.zero;

		}
	}

	void UpdateCrowds()
	{
		for(int i = 0; i < crowds.Length; i++)
		{
			UpdateCrowd(i);
		}
	}

	void UpdateCrowd(int crowdIndex)
	{
		Rigidbody crowd = crowds[crowdIndex];
		if (crowd == null)
			return;

		if (crowd.position.y < -5.0f)
		{
			crowd.position = RandomCrowdPosition() + Vector3.up * 20.0f;
		}

		Vector4 crowdData = crowdsData[crowdIndex];
		
		if (Time.time > crowdData.w)
		{
			crowdData.w = Time.time + RandomRange(crowdRefreshTimeRange);
			Vector2 circle = Random.insideUnitCircle * 10.0f;

			Vector3 nextPosition = crowd.position + new Vector3(circle.x, 0.0f, circle.y);
			if (Mathf.Abs(nextPosition.x) > crowdSpawnExtents.x
				|| Mathf.Abs(nextPosition.z) > crowdSpawnExtents.z
				|| Random.value < 0.1f)
			{
				nextPosition = RandomCrowdPosition();
			}

			crowdData = Set3(crowdData, nextPosition);
			crowdsData[crowdIndex] = crowdData;
		}
		// move legs
		float timeLeft = (Time.time - crowdData.w);
		Transform leftLeg = crowd.transform.GetChild(PART_LEG_L);
		Transform rightLeg = crowd.transform.GetChild(PART_LEG_R);

		float height = 0.677f;
		float upHeight = 1f;

		float fore = 0.0f;

		float heightL = Mathf.Lerp(height, upHeight, Mathf.Cos(timeLeft * 20.0f) * 0.5f + 0.5f);
		float foreL = fore + Mathf.Cos(timeLeft * 20.0f - Mathf.PI * 0.5f) * 0.1f;
		Vector3 leftLegPos = leftLeg.localPosition;
		leftLegPos.y = heightL;
		leftLegPos.z = foreL;

		leftLeg.localPosition = leftLegPos;

		Vector3 rightLegPos = rightLeg.localPosition;
		float heightR = (upHeight - heightL) + height;
		rightLegPos.y = heightR;
		rightLegPos.z = -foreL;
		rightLeg.localPosition = rightLegPos;

		// move

		Vector3 pos = crowd.position;
		Vector3 dest = Get3(crowdData);
		Vector3 diff = dest - pos;
		float distSqr = diff.sqrMagnitude;


		Vector3 totalForce = Vector3.zero;

		float playerFollow = 0.0f;

		// movement towards the players
		if (gameStarted)
		{
			for (int playerIndex = 0; playerIndex < players.Length; playerIndex++)
			{
				if (players[playerIndex] == null)
					continue;

				Vector3 playerPos = players[playerIndex].position;
				float playerDist = Vector3.Distance(playerPos, pos);
				Vector3 playerDir = (playerPos - pos);
				playerDir.y = 0.0f;
				playerDir.Normalize();
				float playerLerp = Mathf.InverseLerp(playerStats[playerIndex].y, 1.0f, playerDist);
				if (playerDist > 2.0f)
				{
					totalForce += playerDir * RandomRange(forceRange) * playerLerp;


					playerFollow += playerLerp;
				}

			}
		}
		// movement towards the destination
		if (distSqr >= 1.0f && playerFollow < 1.0f)
		{
			Vector3 dir = diff.normalized;
			totalForce += dir * RandomRange(forceRange) * (1.0f - playerFollow);
		}

		crowd.AddForce(totalForce, ForceMode.Force);

		// face the velocity if we're moving
		Vector3 v = crowd.velocity;
		v.y = 0.0f;
		if (v.sqrMagnitude > 0.001f)
		{
			Vector3 dir = v.normalized;
			Quaternion rot = crowd.rotation;
			rot = Quaternion.Slerp(rot, Quaternion.LookRotation(dir), Time.deltaTime * crowdTurnLerpRate);

			crowd.MoveRotation(rot);
		}

		// limit on speed
		float speedSqr = crowd.velocity.sqrMagnitude;

		if (speedSqr > maxSpeedSquared)
		{
			crowd.velocity = crowd.velocity.normalized * maxSpeed;
		}


	}



	private Vector3 RandomCrowdPosition()
	{
		return new Vector3(
			Random.Range(-1.0f, 1.0f) * crowdSpawnExtents.x,
			0.0f,
			Random.Range(-1.0f, 1.0f) * crowdSpawnExtents.z
			);
	}

	#endregion

	//--------------------------------------------------
	#region Pickups

	public Transform heartPrefab;

	public List<Transform> pickups = new List<Transform>();
	public Vector2 pickupIntervalRange = new Vector2(6.0f, 12.0f);
	private float nextPickupTime = 6.0f;
	public ParticleSystem heartParticleSystem;
	public AudioClip heartPickupSound;
	private System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();

	void UpdatePickups()
	{
		if (Time.time > nextPickupTime)
		{
			nextPickupTime = Time.time + RandomRange(pickupIntervalRange);
			Vector3 pos = RandomCrowdPosition();
			pos.y = 2.5f;

			Transform pickup = Instantiate<Transform>(heartPrefab, pos, heartPrefab.rotation);
			pickups.Add(pickup);
		}
		for(int i = 0; i < pickups.Count; i++)
		{
			if (pickups[i] == null) continue;
			pickups[i].rotation *= Quaternion.Euler(0.0f, 0.0f, 60.0f * Time.deltaTime);
		}

		// check if close enough to any player
		for(int i = 0; i < players.Length; i++)
		{
			if (players[i] == null)
				continue;

			Vector3 playerPos = players[i].position;
			for(int j = pickups.Count-1; j >= 0; j--)
			{
				Transform pickup = pickups[j];
				if (pickup == null) continue;

				if (Vector3.Distance(pickup.position, playerPos) < 2f)
				{
					Vector3 stats = playerStats[i];
					stats.z += 1.0f;
					playerStats[i] = stats;
					pickups.RemoveAt(j);
					Destroy(pickup.gameObject);
					ShowPickupPickedUp(pickup.position);
					stringBuilder.Length = 0;
					int score = Mathf.RoundToInt(stats.z);
					for(int k = 0; k < score; k++)
					{
						stringBuilder.Append("♥");
					}

					stringBuilder.Append(" <color=#ffffffff>");
					stringBuilder.Append(score.ToString());
					stringBuilder.Append("</color>");

					scoreSliders[i].text = stringBuilder.ToString();
					continue;
				}
			}
		}
	}
	private void ShowPickupPickedUp(Vector3 pos)
	{
		Debug.Log("Pickup picked up");
		heartParticleSystem.transform.position = pos;
		heartParticleSystem.Emit(10);

		AudioSource.PlayClipAtPoint(heartPickupSound, pos);
	}
	#endregion

	//--------------------------------------------------
	#region Utility

	private static Vector4 Set3(Vector4 vector4, Vector3 pos)
	{
		vector4.x = pos.x;
		vector4.y = pos.y;
		vector4.z = pos.z;
		return vector4;
	}

	private static Vector3 Get3(Vector4 vector4)
	{
		Vector3 result = Vector3.zero;
		result.x = vector4.x;
		result.y = vector4.y;
		result.z = vector4.z;
		return result;
	}

	private static float RandomRange(Vector2 range)
	{
		return Random.Range(range.x, range.y);
	}
	#endregion
}
